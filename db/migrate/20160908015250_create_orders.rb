	class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :number
      t.decimal :price, precision: 12, scale: 2, default: 0

      t.timestamps null: false
    end
    add_index :orders, :number, unique: true
  end
end
