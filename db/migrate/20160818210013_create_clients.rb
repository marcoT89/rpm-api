class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name, null: false
      t.string :email
      t.string :phone
      t.string :address
      t.string :city
      t.string :state

      t.timestamps null: false
    end
  end
end
