class AddClientRefToClient < ActiveRecord::Migration
  def change
    add_column :clients, :referred_by, :integer, index: true
    add_foreign_key :clients, :clients, column: :referred_by
  end
end
