class AddStatusToClients < ActiveRecord::Migration
  def change
    add_column :clients, :status, :integer, default: 0, null: false
  end
end
