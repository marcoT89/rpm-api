# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = User.create name: 'Administrador', email: 'admin@gmail.com', password: '123456'
puts "#{admin.name} criado com sucesso"

# Produtos
skilet = Product.create name: 'Frigideira de 20cm', description: 'Descrição da frigideira de 20cm', price: 1990
puts "#{skilet.name} adicionado a lista de produtos"

# Clientes
marco = Client.first_or_create name: 'Marco Túlio', email: 'marcotulio.avila@gmail.com'
puts "Cliente #{marco.name} criado com sucesso"

# Orders
order = Order.first_or_create number: 1, price: '2000'
order.products << Product.first
puts "Pedido nº #{order.number} criado com #{skilet.name}"

case Rails.env
when 'test'
    user = User.create name: 'Admin', email: 'admin@gmail.com', password: '123456'
    puts "#{user.name} criado com sucesso"
    user = User.create name: 'John', email: 'john@gmail.com', password: '123456'
    puts "#{user.name} criado com sucesso"
    user = User.create name: 'Gib', email: 'gib@gmail.com', password: '123456'
    puts "#{user.name} criado com sucesso"
    user = User.create name: 'Joe', email: 'joe@gmail.com', password: '123456'
    puts "#{user.name} criado com sucesso"
    user = User.create name: 'Lucy', email: 'lucy@gmail.com', password: '123456'
    puts "#{user.name} criado com sucesso"
    user = User.create name: 'Anna', email: 'anna@gmail.com', password: '123456'
    puts "#{user.name} criado com sucesso"
end
