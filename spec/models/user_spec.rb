require 'rails_helper'

describe User, type: :model do
  before(:each) do
    @user = User.first
  end
  describe 'can method' do
    it 'should return a boolean if user has the specific permission' do
      expect(@user.name.nil?).to be false
      expect(@user.permissions).not_to be_nil
      expect(@user.can?(:update_user)).to be true
      expect(@user.have_permission?(:update_user)).to be true
      expect(@user.can?(:some_other_permission)).to be false
    end
  end
end