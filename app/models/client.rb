class Client < ActiveRecord::Base
  validates :name, presence: { message: 'not_blank' }
  has_many :orders
  has_many :products, through: :orders
  belongs_to :user
  belongs_to :referrer, class_name: 'Client', foreign_key: :referred_by
  has_many :referrals, class_name: 'Client', foreign_key: :referred_by

  enum status: [:not_contacted, :contacted, :contacted_by_client, :not_interested, :happy]

  scope :all_clients, -> { joins(:orders) }
  scope :all_referrals, -> { where.not(id: all_clients.ids) }
end
