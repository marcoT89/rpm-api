class Order < ActiveRecord::Base
  belongs_to :client
  has_and_belongs_to_many :products

  # validates :number, presence: { message: 'not_blank' }
  validates_presence_of :price, message: 'not_blank'
  validates_presence_of :number, message: 'not_blank'
end
