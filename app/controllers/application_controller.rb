class ApplicationController < ActionController::API
  include Knock::Authenticable

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  rescue_from ActionController::ParameterMissing, :with => :parameter_missing
  rescue_from ActiveModel::ForbiddenAttributesError, :with => :forbidden_attributes_error
  rescue_from ActiveRecord::RecordNotUnique, :with => :record_already_exists

private
  def record_not_found
    render json: {message: "record_not_found"}, status: :not_found
  end

  def parameter_missing
    render json: {message: "parameter_missing"}, status: :bad_request
  end

  def forbidden_attributes_error
    render json: {message: "forbidden_attributes_error"}, status: :not_acceptable
  end

  def record_already_exists
    render json: {message: "record_already_exists"}, status: :not_acceptable
  end
end
