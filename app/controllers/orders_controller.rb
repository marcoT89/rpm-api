class OrdersController < ApplicationController
  before_action :authenticate_user

  def index
    render json: Order.all, include: :products
  end

  def show
    render json: Order.find(params.require(:id)), include: :products
  end

  def destroy
    Order.find(params.require(:id)).destroy
    render json: {}, status: :no_content
  end

  # POST /orders
  def create
    @order = Order.create order_params
    return render json: @order.errors, status: :not_acceptable if @order.invalid?
    render json: @order, status: :created
  end

  def update
    @order = Order.find(params.require(:id))
    if @order.update_attributes(order_params)
      render json: @order, status: :ok
    else
      render json: @order.errors, status: :not_acceptable
    end
  end

  private
  def order_params
    params.require(:order).permit(:number, :price)
  end
end
