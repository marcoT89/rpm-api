class ClientsController < ApplicationController
  before_action :authenticate_user

  def create
    @client = current_user.clients.create client_params
    return render json: @client.errors, status: :not_acceptable if @client.invalid?
    render json: @client, status: :created
  end

  def update
    @client = current_user.clients.find(params.require(:id))
    if @client.update_attributes(client_params)
      render json: @client, status: :ok
    else
      render json: @client.errors, status: :not_acceptable
    end
  end

  def destroy
    current_user.clients.find(params.require(:id)).destroy
    render json: {}, status: :no_content
  end

  def list_all
    render json: current_user.clients.all_clients.reverse
  end

  def list_referrals
    render json: current_user.clients.all_referrals, include: [:referrer]
  end

  def show
    render json: current_user.clients.find(params.require(:id)), include: [:user, :referrer, :referrals, orders: {include: [:products]}]
  end

  private
  def client_params
    params.require(:client).permit(:name, :email, :phone, :city, :address, :state)
  end
end
